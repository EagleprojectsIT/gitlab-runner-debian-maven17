FROM maven:3-amazoncorretto-17-debian

RUN apt-get update && \
    apt-get install -y curl

RUN curl -fsSL get.docker.com | sh

RUN curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb"

RUN apt install -fy ./gitlab-runner_amd64.deb

#RUN apt install -y openjdk-17-jdk maven

RUN apt-get clean

CMD ["gitlab-runner", "run"]
